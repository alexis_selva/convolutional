These are the programming assignments relative to Deep Learning - Convolutional Neural Networks (4th part):

- Assignment 1.1: Convolutional Model: step by step
- Assignment 1.2: Convolutional Model: application
- Assignment 2.1: Keras Tutorial - The Happy House
- Assignment 2.2: Residual Networks
- Assignment 3.1: Car detection with YOLOv2
- Assignment 4.1: Art Generation with Neural Style Transfer
- Assignment 4.2: Face Recognition for the Happy House

For more information, I invite you to have a look at https://www.coursera.org/learn/convolutional-neural-networks
